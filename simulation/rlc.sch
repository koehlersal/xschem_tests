v {xschem version=3.4.4 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
N 80 -460 80 -440 {
lab=A}
N 80 -460 210 -460 {
lab=A}
N 210 -460 210 -410 {
lab=A}
N 80 -380 80 -330 {
lab=B}
N 80 -270 80 -230 {
lab=C}
N 80 -170 80 -150 {
lab=0}
N 80 -150 210 -150 {
lab=0}
N 210 -350 210 -150 {
lab=0}
C {devices/capa.sym} 80 -410 0 0 {name=C1
m=1
value=50nF
footprint=1206
device="ceramic capacitor"}
C {devices/res.sym} 210 -380 0 0 {name=R1
value=1k
footprint=1206
device=resistor
m=1}
C {devices/ind.sym} 80 -300 0 0 {name=L1
m=1
value=10mH
footprint=1206
device=inductor}
C {devices/vsource_arith.sym} 80 -200 0 0 {name=E1 VOL="3*cos(time*time*time*1e11)"}
C {devices/lab_pin.sym} 210 -460 2 0 {name=p1 sig_type=std_logic lab=A}
C {devices/lab_pin.sym} 80 -360 2 0 {name=p2 sig_type=std_logic lab=B}
C {devices/lab_pin.sym} 80 -250 2 0 {name=p3 sig_type=std_logic lab=C}
C {devices/lab_pin.sym} 210 -150 2 0 {name=p4 sig_type=std_logic lab=0}
C {devices/code_shown.sym} 340 -400 0 0 {name=SPICE 
only_toplevel=false 
value="

.tran 10n 2000u uic
.save all

"}
C {devices/title.sym} 160 -40 0 0 {name=l2 author="Martin Köhler"}
