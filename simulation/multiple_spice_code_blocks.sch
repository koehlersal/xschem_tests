v {xschem version=3.4.4 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
N 150 -380 240 -380 {
lab=VDD}
N 40 -280 70 -280 {
lab=I}
N 70 -380 70 -280 {
lab=I}
N 70 -380 110 -380 {
lab=I}
N 70 -190 110 -190 {
lab=I}
N 70 -280 70 -200 {
lab=I}
N 70 -200 70 -190 {
lab=I}
N 150 -350 150 -220 {
lab=Q}
N 150 -280 230 -280 {
lab=Q}
N 150 -160 150 -120 {
lab=GND}
N 150 -190 240 -190 {
lab=GND}
N 240 -190 240 -140 {
lab=GND}
N 150 -140 240 -140 {
lab=GND}
N 150 -430 150 -410 {
lab=VDD}
N 240 -420 240 -380 {
lab=VDD}
N 150 -420 240 -420 {
lab=VDD}
C {devices/vsource.sym} 450 -310 0 0 {name=V1.8 value="pwl 0 1.8"}
C {devices/gnd.sym} 450 -280 0 0 {name=l1 lab=GND}
C {devices/lab_pin.sym} 450 -340 1 0 {name=p1 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 150 -430 1 0 {name=p2 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 40 -280 0 0 {name=p5 sig_type=std_logic lab=I}
C {sky130_fd_pr/pfet_01v8.sym} 130 -380 0 0 {name=M1
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} 230 -280 2 0 {name=p3 sig_type=std_logic lab=Q}
C {sky130_fd_pr/nfet_01v8.sym} 130 -190 0 0 {name=M2
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {devices/gnd.sym} 150 -120 0 0 {name=l2 lab=GND}
C {devices/gnd.sym} 450 -140 0 0 {name=l3 lab=GND}
C {devices/lab_pin.sym} 450 -200 2 0 {name=p6 sig_type=std_logic lab=I}
C {devices/vsource.sym} 450 -170 0 0 {name=V.100MHz value="pulse(0 1.8 1ns 1ns 1ns 5ns 10ns)"}
C {devices/code_shown.sym} 50 -750 0 0 {name=SPICE_PARAMS_PLACEHOLDER
only_toplevel=false 
value="
.lib /p/anagen/bag2/ws.pub.analog.opensource/common/pdk/sky130A/libs.tech/ngspice/sky130.lib.spice tt
.param mc_mm_switch=0 
"}
C {devices/code_shown.sym} 50 -600 0 0 {name=SPICE_MAIN
only_toplevel=false 
value="
.tran 0.1n 1u
.save all
"}
