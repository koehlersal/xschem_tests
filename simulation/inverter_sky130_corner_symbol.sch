v {xschem version=3.4.4 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
N 150 -310 240 -310 {
lab=VDD}
N 40 -210 70 -210 {
lab=I}
N 70 -310 70 -210 {
lab=I}
N 70 -310 110 -310 {
lab=I}
N 70 -120 110 -120 {
lab=I}
N 70 -210 70 -130 {
lab=I}
N 70 -130 70 -120 {
lab=I}
N 150 -280 150 -150 {
lab=Q}
N 150 -210 230 -210 {
lab=Q}
N 150 -90 150 -50 {
lab=GND}
N 150 -120 240 -120 {
lab=GND}
N 240 -120 240 -70 {
lab=GND}
N 150 -70 240 -70 {
lab=GND}
N 150 -360 150 -340 {
lab=VDD}
N 240 -350 240 -310 {
lab=VDD}
N 150 -350 240 -350 {
lab=VDD}
C {devices/vsource.sym} 340 -230 0 0 {name=V1.8 value="pwl 0 1.8"}
C {devices/gnd.sym} 340 -200 0 0 {name=l1 lab=GND}
C {devices/lab_pin.sym} 340 -260 1 0 {name=p1 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 150 -360 1 0 {name=p2 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 40 -210 0 0 {name=p5 sig_type=std_logic lab=I}
C {sky130_fd_pr/pfet_01v8.sym} 130 -310 0 0 {name=M1
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} 230 -210 2 0 {name=p3 sig_type=std_logic lab=Q}
C {sky130_fd_pr/nfet_01v8.sym} 130 -120 0 0 {name=M2
L=0.15
W=1
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {devices/gnd.sym} 150 -50 0 0 {name=l2 lab=GND}
C {devices/gnd.sym} 340 -60 0 0 {name=l3 lab=GND}
C {devices/lab_pin.sym} 340 -120 2 0 {name=p6 sig_type=std_logic lab=I}
C {devices/code_shown.sym} 50 -670 0 0 {name=SPICE 
only_toplevel=false 
value="* NOTE: this is based on 
* https://github.com/bluecmd/learn-sky130/blob/main/schematic/xschem/getting-started.md
********************************************************************************************
* Prerequisite: envionment variables set:
*   export SPICE_SCRIPTS=$PDK_ROOT/$PDK/libs.tech/ngspice/   # ngspice loads spinit from here
*   export NGSPICE_INPUT_DIR=$SPICE_SCRIPTS   # ngspice will search *.lib|*.cir|*.inc here
********************************************************************************************

.lib sky130.lib.spice tt
* .param mc_mm_switch=0 

.tran 0.1n 1u
.save all
"}
C {devices/vsource.sym} 340 -90 0 0 {name=V.100MHz value="pulse(0 1.8 1ns 1ns 1ns 5ns 10ns)"}
C {sky130_fd_pr/corner.sym} 590 -380 0 0 {name=CORNER only_toplevel=false corner=tt}
